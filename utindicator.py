#!/usr/bin/env python
#coding=utf-8

import appindicator
import gtk
import os
import ConfigParser
from subprocess import call
import webbrowser

PING_FREQUENCY = 5 # seconds

config = ConfigParser.ConfigParser()
config.readfp(open(os.getcwd() + '/config.ini'));

jenkins_url = config.get('settings','jenkinsurl')

utindicator = appindicator.Indicator('utindicator', '', appindicator.CATEGORY_APPLICATION_STATUS)
utindicator.set_status(appindicator.STATUS_ACTIVE)

def open_jenkins(widget):
	webbrowser.open(jenkins_url)

def check_status():
	status_file = config.get('settings','statusfile')
	status_file = open(status_file, "r")
	status = status_file.readline()

	if status == '1':
		utindicator.set_icon(os.getcwd() + '/pass.png')
		utindicator.set_status(appindicator.STATUS_ATTENTION)
	else:
		
		utindicator.set_icon(os.getcwd() + '/fail.png')
		utindicator.set_status(appindicator.STATUS_ACTIVE)

	gtk.timeout_add(PING_FREQUENCY * 1000, check_status)

check_status()

m = gtk.Menu()

go_to_jenkins = gtk.MenuItem("Go to Jenkins")
go_to_jenkins.show()
go_to_jenkins.connect("activate", open_jenkins)
m.append(go_to_jenkins)

utindicator.set_menu(m)

gtk.main()

